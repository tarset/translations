#Firebase Android Codelab
##1. Огляд
Ласкаво просимо в лабораторію коду Friendly Chat. У цій лабораторії коду Ви дізнаєтеся, як використовувати платформу Firebase для створення андроїд-додатків. Ви реалізуєте чат-клієнт і контролюватимете його роботу за допомогою Firebase.

###Що Ви навчитеся робити:
- Дозволяти користувачам увійти в систему.
- Синхронізовувати дані за допомогою бази даних Firebase в реальному часі.
- Отримувати сповіщення з Firebase Notifications.
- Налаштовувати програму з Firebase Remote Config.
- Відслідковувати потоки користування додатком з Firebase Analytics.
- Дозволяти користувачам надсилати запрошення на встановлення з Firebase Invites.
- Відображати рекламу з AdMob.
- Отримувати звіти аварій з Firebase Crash Reporting.
- Тестувати Ваш додаток з Firebase Test Lab.

###Що від Вас вимагається:
- [Android Studio](https://developer.android.com/sdk/installing/studio.html) версії 2.1+.
- Зразок коду.
- Пристрій або емулятор для тестування з Android 2.3+ та Google Play services версія 9.8 або пізніша.
- [Google app](https://play.google.com/store/apps/details?id=com.google.android.googlequicksearchbox) версії 6.6+ (лише потрібно для тестування Firebase App Indexing в кроці 9).
- Якщо використовуєте пристрій, потрібний кабель для підключення.

##2. Отримання зразка коду
Клонуйте репозиторій GitHub командною стрічкою:  
`
$ git clone https://github.com/firebase/friendlychat
`

Репозиторій "friendly" містить малі зразкові проекти. В цій лабораторії коду використовувати будете лише  
**android-start** - початковий код, що Ви зконстроюєте в цій лабораторії коду.  
**android** - завершений код для кінцевого зразкового додатку.

_**Примітка:** Якщо Ви хочете запустити кінцевий додаток, Ви можеш створити проект в консолі Firebase з відповідними назвою та SHA1 пакету. Гляньте Create Firebase в консолі проекту для команди. Крім того, доведеться включити Google і Auth Provider; зробити це в розділі Authentication в консолі Firebase._

##3. Імпорт початкового додатку
В Android Studio виберіть директорію android-start скачаного зразкового коду  (**File > Open >** .../firebase-codelabs/android-start).

Ви повинні зараз мати відкритий проект android-start в Android Studio.

##4. Створення проекту в консолі Firebase
1. Перейдіть в консоль Firebase
2. Виберіть **Create New Project** та введіть назву Вашого проекту "FriendlyChat".

###Підключити андроїд-додаток
1. У вікні Overview Вашого нового проекту виберіть **Add Firebase to your Android app**
2. Введіть назву пакету лабораторії коду: `com.google.firebase.codelab.friendlychat`
3. Введіть ваш підпис SHA1 сховища ключів. Якщо Ви використовуєте стандартне налагоджене сховище ключів, використайте цю команду, щоб знайти хеш SHA1:
`keytool -exportcert -alias androiddebugkey -keystore ~/.android/debug.keystore -list -v -storepass android`

_**Примітка:** Ваше налагоджене сховище ключів зазвичай називають "debug.keystore". Як правило, знаходиться в  
<home>/.android/debug.keystore.  
Перегляньте [тут](https://developers.google.com/android/guides/client-auth) для більш детальної інформації про пошук SHA1._

###Додати файл google-services.json у додаток
Після додавання назви пакету і SHA1 та вибору **Continue**, Ваш браузер автоматично завантажує конфігураційний файл, який містить всі необхідні метадані Firebase для Вашого застосунку.

###Додати планіг google-services у додаток
Плагін google-services використовує файл google-services.json, щоб налаштувати додаток для використання Firebase. Наступний рядок повинний бути доданий в кінець файлу build.gradle в каталозі *app* вашого проекту (перевірте, щоб підтвердити):  
`
apply plugin: 'com.google.gms.google-services'
`

###Сихронізація проекту з Gradle файлами
Для того, щоб бути впевненим, що вся залежність доступна для вашого застосування, Ви повинні сихронізувати проект з Gradle файлами в цій точці. Виберіть **Sync Project with Gradle Files** з панелі інструментів Android Studio.

_**Примітка:** Для цієї лабораторії коду Ви повинні використовувати тільки Sync Project з Gradle файлами на даному моменті._

##5. Запуск початкового додатку
Тепер, коли Ви імпортували проект в Android Studio і налаштували плагін `google-services` з файлом JSON, Ви готові запустити додаток вперше. Підключіть Ваш Android пристрій, і натисніть **Run** в панелі інструментів Android Studio.

Додаток повинен запуститися на девайсі. На даний момент, Ви повинні побачити порожній список повідомлень, а також відправлення і прийом повідомлень не буде працювати. У наступному розділі аунтифікуєте користувачів, щоб вони могли використовувати Friendly Chat.

##6. Увімкнення аунтифікації
Давайте вимагати від користувача, щоб входив перед читанням або розміщенням будь-яких повідомлень в Friendly Chat.

###Firebase Realtime Database Rules
Доступ до бази даних Firebase конфігурується за допомогою набору правил, написаних на мові конфігурації в форматі JSON.

Перейдіть до вашого проекту в консолі Firebase, виберіть **Database**, а потім на вкладці **Rules**. Переконайтеся, що правила, встановлені в базі даних виглядає наступним чином:

```json
{
  "rules": {
    ".read": "auth != null",
    ".write": "auth != null"
  }
}
```

Для отримання більш докладної інформації про те, як це працює (включаючи документацію по змінний "auth") можна знайти в [документації безпеки Firebase](https://firebase.google.com/docs/database/security/quickstart).

###Набори API для аутентифікації
Перед тим як додаток може отримати доступ до Firebase Authentication API від імені Ваших користувачів, Ви повинні включити його

1. Перейдіть до [консолі Firebase](http://console.firebase.google.com/) і виберіть проект
2. Виберіть **Authentication**
3. Виберіть вкладку **Sign In Method**
4. Переведіть перемикач **Google** для включення (синій)
5. Натисніть **Save** на діалоговому вікні.

Якщо Ви пізніше отримуєте помилки в цій лабораторії коду з повідомленням «CONFIGURATION_NOT_FOUND», поверніться до цього кроку і двічі перевірте Вашу роботу.

###Добавити залежність Firebase Auth
Firebase-аутентифікація дозволяє легко керувати ідентифікованими користувачами Вашої програми. Підтвердити існування цієї залежності у Вашому файлі `app/build.gradle`.

###app/build.gradle
`
compile 'com.google.firebase:firebase-auth:10.2.0'
`

Додайте змінні екземпляра Auth в клас `MainActivity`:
###MainActivity.java (змінна екземпляра)

```java
// Firebase instance variables
private FirebaseAuth mFirebaseAuth;
private FirebaseUser mFirebaseUser;
```

###Перевірте поточного користувача
Модифікуйте `MainActivity.java`, щоб відправити користувачів на екран входу, коли вони відкривають додаток і є нерозпізнаними.

Додайте наступний до `onCreate` **після** `mUsername` метод ініціалізації:

###MainActivity.java

```java
// Initialize Firebase Auth
mFirebaseAuth = FirebaseAuth.getInstance();
mFirebaseUser = mFirebaseAuth.getCurrentUser();
if (mFirebaseUser == null) {
    // Not signed in, launch the Sign In activity
    startActivity(new Intent(this, SignInActivity.class));
    finish();
    return;
} else {
    mUsername = mFirebaseUser.getDisplayName();
    if (mFirebaseUser.getPhotoUrl() != null) {
        mPhotoUrl = mFirebaseUser.getPhotoUrl().toString();
    }
}
```

Потім додайте новий випадок до `onOptionsItemSelected()` для обробки кнопки входу з:

###MainActivity.java

```java
@Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.sign_out_menu:
                mFirebaseAuth.signOut();
                Auth.GoogleSignInApi.signOut(mGoogleApiClient);
                mUsername = ANONYMOUS;
                startActivity(new Intent(this, SignInActivity.class));
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
```

Тепер у нас є вся логіка, щоб відправити користувача на екран входу в разі потреби. Далі нам потрібно реалізувати вхід в екран, щоб правильно виконати перевірку справжності користувачів.